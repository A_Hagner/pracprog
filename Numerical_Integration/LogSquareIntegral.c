#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_integration.h>



double lnsq(double x, void * params){
double lnsq=log(x)/sqrt(x);
return lnsq;}


int main(){

size_t limit=1000;

gsl_integration_workspace * w = gsl_integration_workspace_alloc(limit);
double result, error;

gsl_function g;
g.function=&lnsq;
g.params = NULL;

double epsabs=1e-6, epsrel=1e-6;


gsl_integration_qags(&g, 0, 1, epsabs, epsrel, limit, w, &result, &error);
printf("The result of integrating log(x)/sqrt(x) from 0 to 1 = %g \n ",result);
//printf("error estimation = %g \n",error);


gsl_integration_workspace_free(w);
return 0;	}
