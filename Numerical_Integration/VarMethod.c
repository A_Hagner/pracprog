#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_integration.h>



double norm_fun(double x, void * params){
double a=*(double *) params;
return exp(-a*x*x);	}


double hamilton_fun(double x, void* params){
double a=*(double *) params;
return (-a*a*x*x/2+a/2+x*x/2)*exp(-a*x*x);}


int main(){
double a;

double epsabs=1e-6, epsrel=1e-6;

size_t limit=1000;

gsl_integration_workspace * wnorm = gsl_integration_workspace_alloc(limit);
gsl_integration_workspace * whamil = gsl_integration_workspace_alloc(limit);

double result_norm, error_norm;
gsl_function norm;
norm.function=&norm_fun;

double result_hamil, error_hamil;
gsl_function hamil;
hamil.function=&hamilton_fun;


for(a=0.05;a<5;a+=0.05){
norm.params = &a;
hamil.params=&a;

gsl_integration_qagi(&norm, epsabs, epsrel, limit, wnorm, &result_norm, &error_norm);
gsl_integration_qagi(&hamil, epsabs, epsrel, limit, whamil, &result_hamil, &error_hamil);
double result=result_hamil/result_norm;

printf("%g\t%g\n",a,result);}//for løkke stop


gsl_integration_workspace_free(whamil);
gsl_integration_workspace_free(wnorm);


return 0;}
