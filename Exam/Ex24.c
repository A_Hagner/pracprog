#include<gsl/gsl_odeiv2.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<assert.h>

//Arccotangent using ODE

int arctan_ode(double x, const double y[], double yprime[], void * params){
	(void)(x); //Avoid unused parameter warning

	yprime[0]=1/(x*x+1); //The diff. equation

return GSL_SUCCESS;}


 //Takes arctan in x
double arctan(double x){
	gsl_odeiv2_system arc;
	arc.function = arctan_ode;
	arc.jacobian = NULL;
	arc.dimension = 1;
	arc.params = NULL;
	
	double initial_step_size=copysign(0.01,x);
	double absolute_accuracy=0, relative_precision=1e-6;
	
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new(&arc, gsl_odeiv2_step_rkf45,initial_step_size,absolute_accuracy,relative_precision);

	double y[1]={0}; //start values
	double t=0.0;
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
return y[0];}



int main(){ 
	int i;
	int step=1, maxstep=100;
	printf("x \t arctan(x) \t atan(x)\n");
	double r=8.0; //range

for(i=0;i<maxstep+1;i+=step){
	double x=-r+2*r*i/maxstep;
printf("%g \t %g \t %g \n",x,arctan(x),atan(x));}

return 0;}

















