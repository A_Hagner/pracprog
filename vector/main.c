#include"nvector.h"
#include<stdio.h>
#include<stdlib.h>
#define RND (double)rand()/RAND_MAX

int main(){

int n=3;

printf("TEST: nvector_alloc \n");
nvector* v=nvector_alloc(n);
if(v==NULL){printf("TEST: FAILED \n \n");}
else{printf("TEST: PASSED \n \n");}


printf("TEST: nvector_set and nvector_get \n");
double value=RND;
int i = 1;
nvector_set(v,i,value);
double gi= nvector_get(v,i);
if(gi==value){printf("TEST: PASSED\n\n");}
else{printf("TEST: FAILED\n\n");}

printf("TEST: nvector_dot_product \n");

nvector* x=nvector_alloc(n);
nvector* y=nvector_alloc(n); 
double z;

for(int j=0; j<n; j++){
	nvector_set(x,j,RND);
	nvector_set(y,j,RND);
	z += (x->data[j])*(y->data[j]);
			}

double dp=nvector_dot_product(x,y);


printf("Real Dot-product(x,y)= %g\n nvector_dot_product(x,y)=%g\n",z,dp);

if(z==dp){printf("TEST: PASSED\n\n");}
else{printf("TEST: FAILED\n\n");}



nvector_free(v);
nvector_free(x);
nvector_free(y);


return 0;
}





























