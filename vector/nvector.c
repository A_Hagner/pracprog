#include<stdio.h>
#include<stdlib.h>
#include"nvector.h"



nvector* nvector_alloc(int n){
	nvector* v=malloc(sizeof(nvector));
	v->size=n;
	v->data=malloc(n*sizeof(double));
	if(v == NULL) fprintf(stderr,"error in nvector_alloc (empty)\n");
	return v;
}

void nvector_free(nvector* v){
	free(v->data);
	free(v);
	v=NULL;
}

void nvector_set(nvector* v, int i, double value){
v->data[i]=value;
}

double nvector_get(nvector* v, int i){
return v->data[i];
}

double nvector_dot_product(nvector* v, nvector* u){
		
	if(v->size != u->size){fprintf(stderr,"error in nvector_dot_product (diff. size)\n");}	
	double d=0;	
	for(int i=0; i<(v->size); i++){ 
	d+=(v->data[i])*(u->data[i]);
					} 



}

















