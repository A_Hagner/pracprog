#include<stdio.h>
#include<math.h>
#include<complex.h>
#include<tgmath.h>

int main()
{
	double x=5;
	double y=tgamma(x);
		printf("gamma(%g)=%g \n",x,y);

	double a=0.5;
	double b=j0(a);
		printf("besself1(%g)=%g \n",a,b);

	
	double  z=-2;
	double  t=0.5;
	double complex sq=csqrt(z);
	double	sr=creal(sq);
	double  si=cimag(sq);
		printf("sqrt(-2)=%g+i%g \n",sr,si);

	double e=M_E;
	double complex ei=cpow(e,I);
	double	eir=creal(ei);
	double  eii=cimag(ei);
		 printf("exp(i)=%g+%gi \n",eir,eii);

	double pi=M_PI;
	double complex eipi=cpow(e,I*pi);
	double	eipir=creal(eipi);
	double  eipii=cimag(eipi);
		printf("exp(i*pi)=%g+%gi \n",eipir,eipii);

	double complex ie=cpow(I,e);
	double	ier=creal(ie);
	double  iei=cimag(ie);
		printf("i^e=%g+%gi \n",ier,iei);
	
	float x1=0.1111111111111111111111111111;
	double x2=0.1111111111111111111111111111;
	long double x3=0.1111111111111111111111111111L;

		printf("float %.25g \n double %.25lg \n long double %.25Lg \n",x1,x2,x3);





return 0;
}
