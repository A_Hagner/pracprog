#include<gsl/gsl_odeiv2.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_errno.h>


int error_ode(double x, const double y[], double yprime[], void * params){
(void)(x); //Avoid unused parameter warn

yprime[0]=2/sqrt(M_PI)*exp(-x*x);

return GSL_SUCCESS;}


 
double error_func(double xslut){
		
	gsl_odeiv2_system errorf;
	errorf.function = error_ode;
	errorf.jacobian = NULL;
	errorf.dimension = 1;
	errorf.params = NULL;
	
	double initial_step_size=copysign(0.01,xslut);
	double absolute_accuracy=0, relative_precision=1e-6;
	
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new(&errorf, gsl_odeiv2_step_rkf45,initial_step_size,absolute_accuracy,relative_precision);

	double y[1]={0};	
	double x=0;
	gsl_odeiv2_driver_apply(driver,&x,xslut,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];}



int main(int argc, char** argv){  
	//if(argc<3){fprintf(stderr,"\n Error - Not enough input Arguments\n");}
	//if(argc>3){fprintf(stderr,"\n Error - To many input Arguments\n");}
	//Could not make this warning work - keep getting argc as zero		
	

	double a= atof( argv[1] );
	double b = atof( argv[2] );
	double dx = atof( argv[3] );
	
	
	for(double z=a;z<b+dx;z+=dx){printf("%g\t%g \n",z,error_func(z));}


return 0;}





















