#include<gsl/gsl_odeiv2.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_errno.h>

//gsl_odeiv2_step_rkf45 god all-around stepper

int orbit_ode(double x, const double y[], double yprime[], void * params){
double eps = *(double *)params;

yprime[0]=y[1];
yprime[1]=1.0-y[0]+eps*y[0]*y[0];
return GSL_SUCCESS;}


 
int main(int argc, char **argv){
	double eps = atof(argv[1]);
	double ypstart = atof(argv[2]);

	gsl_odeiv2_system orbit;
	orbit.function = orbit_ode;
	orbit.jacobian = NULL;
	orbit.dimension = 2;
	orbit.params = (void *) &eps;
	
	double hstart=1e-3;
	double acc=1e-6, relprec=1e-6;
	
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new(&orbit, 	gsl_odeiv2_step_rkf45,hstart,acc,relprec);
	
	double y[2]={1, ypstart};	
	double x=0;
	
	double phi_max=20*M_PI;
	double delta=0.1;
	printf("x\ty\n");

	for (double phi=0; phi<phi_max; phi+=delta){
		int status=gsl_odeiv2_driver_apply (driver, &x, phi, y);
		printf ("%g\t%g\n", 1/y[0]*cos(phi), 1/y[0]*sin(phi));
		if(status != GSL_SUCCESS){fprintf (stderr, "Function-Status=%i", status);}
		}



	gsl_odeiv2_driver_free(driver);
	return 0;}
