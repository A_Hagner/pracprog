#include<gsl/gsl_odeiv2.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_errno.h>

//gsl_odeiv2_step_rkf45 god all-around stepper

int simple_ode(double x, const double y[], double yprime[], void * params){
(void)(x); //Avoid unused parameter warn

yprime[0]=y[0]*(1-y[0]);

return GSL_SUCCESS;}


 
double simple_func(double xslut){
	//Define system		
	gsl_odeiv2_system simple;
	simple.function = simple_ode;
	simple.jacobian = NULL;
	simple.dimension = 1;
	simple.dimension= 1;
	simple.params = NULL;
	
	double initial_step_size=0.1;
	double absolute_accuracy=1e-6, relative_precision=1e-6;
	
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new(&simple, gsl_odeiv2_step_rkf45,initial_step_size,absolute_accuracy,relative_precision);

	double y[1]={0.5};	//Start at 0
	double x=0;
	gsl_odeiv2_driver_apply(driver,&x,xslut,y); //xslut is the value evaluted in f(xslut)


	
	gsl_odeiv2_driver_free(driver);
	return y[0];}



int main(){
	
	printf("Calculated logistic function (in x=3) = %g\n",simple_func(3));
	printf("Real value of logistic function in x=3 = 0.9525741268...\n");
return 0;}
