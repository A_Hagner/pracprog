#include <stdio.h>
#include <tgmath.h>
#include <limits.h>
#include <float.h>

int main(){
int i=1;

printf("\n Smallest Integer for different loops \n");

while(i-1<1){i--;}
printf("Value of Min integer(while): %d\n", i);

int k;
for(k=0; k-1<k; k--);

printf("Value of Min integer(for loop): %d\n", k);

int j=1;
do{
j++;
}while(j-1<j);
printf("Value of Min integer(Do while loop): %d\n", j);

return 0;
}
