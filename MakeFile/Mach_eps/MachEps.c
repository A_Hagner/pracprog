#include <stdio.h>
#include <tgmath.h>
#include <limits.h>
#include <float.h>

int main(){

printf("\n Epsilons as defined \n");
float fe=FLT_EPSILON;
printf("Float Epsilon (float precision)=%f \n",fe);
double de=DBL_EPSILON;
printf("Double Epslion (double precision)=%g \n",de);
long double lde=LDBL_EPSILON;
printf("Long Double Epsilon=%Lg \n",lde);

printf("\n Calculated Epsilons \n");

double x=1;
while(1+x!=1){x/=2;} x*=2;

printf("Double Epsilon=%g\n",x);

long double k;
for(k=1; k+1!=1;k/=2){} k*=2;
printf("Long double episoln=%Lg\n",k);

float i=1;
do{i/=2;}while(i+1!=1);
printf("Float epsilon =%f\n",i);


return 0;
}
