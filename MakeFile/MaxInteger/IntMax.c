#include <stdio.h>
#include <tgmath.h>
#include <limits.h>
#include <float.h>

int main(){
int i=1;

printf("\n Max Integer for different loops \n");

while(i+1>1){i++;}
printf("Value of Max integer(while): %d\n", i);

int k;
for(k=1; k+1>k; k++);

printf("Value of Max integer(for loop): %d\n", k);

int j=1;
do{
j++;
}while(j+1>j);
printf("Value of Max integer(Do while loop): %d\n", j);

return 0;
}
