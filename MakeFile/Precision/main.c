#include <stdio.h>
#include <tgmath.h>
#include <limits.h>
#include <float.h>
#include"prec.h"

int main(){
double a=5;
double b=4.89334;
double t=0.1;
double e=0.1;  

printf("\n Equal within relative and absolute precision \n");
printf("Inserted values are: a=%g, b=%g, tau=%g and epsilon=%g\n",a,b,t,e);
prec(a,b,t,e);
}
