#include <stdio.h>
#include <tgmath.h>
#include <limits.h>
#include <float.h>

int prec(double a, double b, double t, double e){
printf("Are a and b equal with absolute precision or relative precision?\n");
if(fabs(a-b)<t){printf("a and b were within either tau or epsilon half\n"); return 1;}
else if(fabs(a-b)/(fabs(a)+fabs(b)) < e/2 ){printf("a and b were equal within either tau or epsilon half\n"); return 1;}
else{printf("a and b were not equal within either tau or epsilon half\n"); return 0;}

}

