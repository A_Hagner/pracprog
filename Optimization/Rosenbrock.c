#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<math.h>

#define simplex gsl_multimin_fminimizer_nmsimplex2

//f(x,y)=(1-x)²+100(y-x²)²
double rosen_fun(const gsl_vector* x, void * params){

	const double x1 = gsl_vector_get(x, 0);
	const double x2 = gsl_vector_get(x, 1);
	double rosen_fun=pow((1-x1),2)+100*pow((x2-x1*x1),2);

	return rosen_fun;}


int main(){
	size_t dim=2;
	gsl_multimin_function g;
	g.f=rosen_fun;
	g.n=dim;
	g.params=NULL;

gsl_multimin_fminimizer * state =
gsl_multimin_fminimizer_alloc(simplex,dim);
gsl_vector *start = gsl_vector_alloc(dim);
gsl_vector *step = gsl_vector_alloc(dim);
gsl_vector_set(start,0,-3); /* x_start */
gsl_vector_set(start,1,2); /* y_start */
gsl_vector_set_all(step,0.1);
gsl_multimin_fminimizer_set (state, &g, start, step);

int iter=0,status;
double acc=0.001;
do{
	iter++;
	int flag = gsl_multimin_fminimizer_iterate (state);
	if(flag!=0)break;
	status = gsl_multimin_test_size (state->size, acc);
	if (status == GSL_SUCCESS) printf ("converged\n");
	printf("iter=%3i\tx= %8f\ty= %8f\tRosenbrock= %8g\tsize= %8g\n",
		iter,gsl_vector_get(state->x,0),gsl_vector_get(state->x,1),state->fval,state->size);

}while(status == GSL_CONTINUE && iter < 999);
	

gsl_vector_free(start);
gsl_vector_free(step);
gsl_multimin_fminimizer_free(state);
return 0;
}




