#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<math.h>

#define simplex gsl_multimin_fminimizer_nmsimplex2
#define f(t) A*exp(-t /T) + B


typedef struct {int n; double *t,*y,*e;} exp_data;


int Henfalds_fun(const gsl_vector * x, void * params){
	double A=gsl_vector_get(x,0);
	double T=gsl_vector_get(x,1);
	double B=gsl_vector_get(x,2);	

	exp_data *p =(exp_data*)params;

	int	n=p->n;
	double *t=p->t;
	double *y=p->y;
	double *e=p->e;
	double sum=0;
	
	for(int i=0;i<n;i++){sum+= pow((f(t[i])-y[i])/e[i],2);}
	
return sum;}


int main(){
double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
int n = sizeof(t)/sizeof(t[0]);

	exp_data params;
	params.n=n;
	params.t=t;
	params.y=y;
	params.e=e;


	size_t dim=3;
	gsl_multimin_function g;
	g.f=Henfalds_fun;
	g.n=dim;
	g.params=(void*)&params;

gsl_multimin_fminimizer * state = gsl_multimin_fminimizer_alloc(simplex,dim);
gsl_vector *start = gsl_vector_alloc(dim);
gsl_vector *step = gsl_vector_alloc(dim);
gsl_vector_set(start,0,1); /* A_start */
gsl_vector_set(start,1,1); /* T_start */
gsl_vector_set(start,2,1.3); /* B_start */

gsl_vector_set_all(step,0.1);
gsl_multimin_fminimizer_set (state, &g, start, step);

int iter=0,status;
double acc=0.001;
do{
	iter++;
	int flag = gsl_multimin_fminimizer_iterate (state);
	if(flag!=0)break;
	status = gsl_multimin_test_size (state->size, acc);
	if (status == GSL_SUCCESS) fprintf (stderr,"converged\n");
	fprintf(stderr,"iter= %3i  ",iter);
	fprintf(stderr,"A= %.4f  ",gsl_vector_get(state->x,0));
	fprintf(stderr,"T= %.4f  ",gsl_vector_get(state->x,1));
	fprintf(stderr,"B= %.4f  ",gsl_vector_get(state->x,2));
	fprintf(stderr,"size= %.2f  ",state->size);
	fprintf(stderr,"fval/n= %.3f  ",state->fval/n);
	fprintf(stderr,"\n");	
}while(status == GSL_CONTINUE && iter < 999);
	

	double A=gsl_vector_get(state->x,0);
	double T=gsl_vector_get(state->x,1);
	double B=gsl_vector_get(state->x,2);

	printf("t\tA*exp(-t/T)+B\n");
	double dt=(t[n-1]-t[0])/50;
	for(double ti=t[0]; ti<t[n-1]+dt; ti+=dt){printf("%g\t%g\n",ti,A*exp(-ti/T)+B);}


gsl_vector_free(start);
gsl_vector_free(step);
gsl_multimin_fminimizer_free(state);
return 0;}




















