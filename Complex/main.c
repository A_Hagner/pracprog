#include"komplex.h"
#include"stdio.h"

int main(){
komplex a = {1,1};
komplex b = {2,3};
printf("TEST: komplex addition \n");
komplex add=komplex_add(a,b);
komplex_print("Komplex_add gives: a+b = ",add);

double x = a.re + b.re;
double y = a.im + b.im;
printf("Actually a+b = %lg + i*%lg \n\n",x,y);


printf("TEST: komplex multiplying \n");
komplex multi=komplex_mul(a,b);
komplex_print("Komplex_mul is a*b =",multi);



return 0;
}
