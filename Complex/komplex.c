#include<stdio.h>
#include"komplex.h"

void komplex_print(char *s, komplex a){
printf("%s %g+i*%g \n",s,a.re,a.im );
}

komplex komplex_new(double x, double y){
komplex z = {x,y};
return z;
}

void komplex_set(komplex* z, double x, double y){
z->re = x;
z->im = y;
}

komplex komplex_add(komplex a, komplex b){
komplex z;
z.re = a.re + b.re;
z.im = a.im + b.im;
return z;
}

komplex komplex_sub(komplex a, komplex b){
komplex z;
z.re = a.re - b.re;
z.im = a.im - b.im;
return z;
}

komplex komplex_mul(komplex a, komplex b){
komplex z;
z.re = a.re * b.re - a.im * b.im;
z.im = a.im*b.re + a.re*b.im;
return z;
}

